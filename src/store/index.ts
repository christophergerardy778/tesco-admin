import Vue from 'vue'
import Vuex from 'vuex'
import Auth from "@/store/auth";
import VuexPersistence from "vuex-persist";
import Preferences from "@/store/preferences";
import Notice from "@/store/notice";
import Web from "@/store/web";
import Upload from "@/store/upload";
import Attempt from "@/store/attempt";

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  key: 'tesco-app',
  storage: window.localStorage,
  modules: ["Auth", "Preferences"]
});

export default new Vuex.Store({
  modules: {
    Auth,
    Preferences,
    Notice,
    Web,
    Upload,
    Attempt
  },

  plugins: [vuexLocal.plugin]
});
