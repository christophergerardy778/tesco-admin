import {Action, Module, VuexModule} from "vuex-module-decorators";
import {GetAttemptsPort} from "@/core/repository/attempt/port/GetAttemptsPort";
import GetAttempts from "@/core/use_cases/attempt/GetAttempts";
import {UpdateAttemptPort} from "@/core/repository/attempt/port/UpdateAttemptPort";
import UpdateAttemptStatus from "@/core/use_cases/attempt/UpdateAttemptStatus";

@Module({
    name: "Attempt",
    namespaced: true
})
export default class Attempt extends VuexModule {
    @Action
    public async getAttempts(port: GetAttemptsPort) {
        const useCase = new GetAttempts();
        return useCase.execute(port);
    }

    @Action
    public async updateAttempt(port: UpdateAttemptPort) {
        const useCase = await new UpdateAttemptStatus();
        return useCase.execute(port);
    }
}