import {Module, VuexModule} from "vuex-module-decorators";
import {WebRouteEntity} from "@/core/entity/web/WebRouteEntity";

@Module({
    name: "Web",
    namespaced: true
})
class Web extends VuexModule {
    public menu: WebRouteEntity[] = [
        {
            icon: 'home',
            text: 'inicio',
            route: '/home'
        },
        {
            icon: 'supervisor_account',
            text: 'Solicitudes',
            route: '/solicitudes'
        },
        {
            icon: 'description',
            text: 'Anuncios',
            route: '/anuncios'
        },
        {
            icon: 'groups',
            text: 'Accesos',
            route: '/accesos'
        }
    ];
}

export default Web;