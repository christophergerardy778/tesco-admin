import {Action, Module, VuexModule} from "vuex-module-decorators";
import GetNotices from "@/core/use_cases/notice/GetNotices";
import NoticeRepository from "@/core/repository/notice/NoticeRepository";
import {GetNoticesPort} from "@/core/repository/notice/port/GetNoticesPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {NoticeEntity} from "@/core/entity/notice/NoticeEntity";
import {GetNoticeByIdPort} from "@/core/repository/notice/port/GetNoticeByIdPort";
import GetNoticeById from "@/core/use_cases/notice/GetNoticeById";
import {CreateNoticePort} from "@/core/repository/notice/port/CreateNoticePort";
import CreateNotice from "@/core/use_cases/notice/CreateNotice";
import {DeleteNoticePort} from "@/core/repository/notice/port/DeleteNoticePort";
import DeleteNoticeById from "@/core/use_cases/notice/DeleteNoticeById";

@Module({
    name: "Notice",
    namespaced: true
})
export default class Notice extends VuexModule {
    @Action
    public getNotices(port: GetNoticesPort) :  Promise<ServerResponse<NoticeEntity[]>>{
        const useCase = new GetNotices(new NoticeRepository());
        return useCase.execute(port);
    }

    @Action
    public async getNotice(port: GetNoticeByIdPort) : Promise<ServerResponse<NoticeEntity>>{
        const useCase = new GetNoticeById(new NoticeRepository());
        return useCase.execute(port);
    }

    @Action
    public async createNotice(port: CreateNoticePort) : Promise<ServerResponse<string>> {
        const useCase = new CreateNotice();
        return useCase.execute(port);
    }

    @Action async deleteNoticeById(port: DeleteNoticePort) : Promise<ServerResponse<string>> {
        const useCase = new DeleteNoticeById();
        return useCase.execute(port);
    }
}