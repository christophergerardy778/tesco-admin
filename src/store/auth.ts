import {Action, Module, Mutation, VuexModule} from "vuex-module-decorators";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import {LoginPort} from "@/core/repository/auth/AuthPort";
import LoginUseCase from "@/core/use_cases/auth/LoginUseCase";

@Module({
    name: "Auth",
    namespaced: true
})
export default class Auth extends VuexModule {
    public user: UserEntity = {
        _id: "",
        name: "",
        email: "",
        lastname: "",
        password: "",
        enrollment: "",
        active: false,
        graduate: false,
        egressDate: undefined,
        dateOfAdmission: undefined
    };

    private token: string = '';

    @Action
    public async loginByEmail(port: LoginPort) {
        const useCase = new LoginUseCase();
        return useCase.execute(port);
    }

    @Mutation
    public setUser(user: UserEntity) {
        this.user = user;
    }

    @Mutation
    public setToken(token: string) {
        this.token = token;
    }

    @Action({ rawError: true })
    public async logout() {
        this.setToken("");
        this.setUser({
            _id: "",
            name: "",
            email: "",
            lastname: "",
            password: "",
            enrollment: "",
            active: false,
            graduate: false,
            egressDate: undefined,
            dateOfAdmission: undefined
        });
    }
}