import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    preset: {
        theme: {
            dark: false,

            themes: {
                light: {
                    primary: '#3DE29D',
                },

                dark: {
                    primary: '#3DE29D'
                }
            }
        }
    }
});
