import {IAttemptRepository} from "@/core/repository/attempt/IAttemptRepository";
import {GetAttemptsPort} from "@/core/repository/attempt/port/GetAttemptsPort";
import {AttemptEntity} from "@/core/entity/attempt/AttemptEntity";
import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import AuthApi from "@/core/network/AuthApi";
import {UpdateAttemptPort} from "@/core/repository/attempt/port/UpdateAttemptPort";

export default class AttemptRepository implements IAttemptRepository {
    getAttemptsByStatusAndPage(port: GetAttemptsPort): Promise<AxiosResponse<ServerResponse<AttemptEntity[]>>> {
        return new AuthApi()
            .getAxios()
            .get('/attempt', {
                params: {
                    page: port.page,
                    status: port.status
                }
            });
    }

    updateAttemptStatus(port: UpdateAttemptPort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .put('/attempt', port);
    }
}