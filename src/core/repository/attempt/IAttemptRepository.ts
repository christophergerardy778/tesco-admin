import {AttemptEntity} from "@/core/entity/attempt/AttemptEntity";
import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {GetAttemptsPort} from "@/core/repository/attempt/port/GetAttemptsPort";
import {UpdateAttemptPort} from "@/core/repository/attempt/port/UpdateAttemptPort";

export interface IAttemptRepository {
    getAttemptsByStatusAndPage(port: GetAttemptsPort) : Promise<AxiosResponse<ServerResponse<AttemptEntity[]>>>;
    updateAttemptStatus(port: UpdateAttemptPort) : Promise<AxiosResponse<ServerResponse<string>>>;
}