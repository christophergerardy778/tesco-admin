import {STATUS_ATTEMPT} from "@/core/entity/attempt/AttemptEntity";

export interface UpdateAttemptPort {
    state: STATUS_ATTEMPT,
    user_id: string;
    attempt_id: string;
}