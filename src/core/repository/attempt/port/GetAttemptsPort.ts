import {STATUS_ATTEMPT} from "@/core/entity/attempt/AttemptEntity";

export interface GetAttemptsPort {
    page: number;
    status: STATUS_ATTEMPT
}