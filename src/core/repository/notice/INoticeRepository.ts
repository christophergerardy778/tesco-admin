import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {NoticeEntity} from "@/core/entity/notice/NoticeEntity";
import {GetNoticesPort} from "@/core/repository/notice/port/GetNoticesPort";
import {GetNoticeByIdPort} from "@/core/repository/notice/port/GetNoticeByIdPort";
import {CreateNoticePort} from "@/core/repository/notice/port/CreateNoticePort";
import {DeleteNoticePort} from "@/core/repository/notice/port/DeleteNoticePort";

export interface INoticeRepository {
    getPaginatedNotices(port: GetNoticesPort) : Promise<AxiosResponse<ServerResponse<NoticeEntity[]>>>;
    getNoticeById(port: GetNoticeByIdPort) : Promise<AxiosResponse<ServerResponse<NoticeEntity>>>;
    createNotice(port: CreateNoticePort) : Promise<AxiosResponse<ServerResponse<string>>>;
    deleteNotice(port: DeleteNoticePort) : Promise<AxiosResponse<ServerResponse<string>>>
}