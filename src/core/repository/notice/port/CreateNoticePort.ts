import {NoticeEntity} from "@/core/entity/notice/NoticeEntity";
import {ILayout} from "@/core/entity/notice/layout/ILayout";

export interface CreateNoticePort {
    user_id: string;
    notice: NoticeEntity;
    layouts: ILayout[];
}