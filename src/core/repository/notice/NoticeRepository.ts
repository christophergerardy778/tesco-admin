import {AxiosResponse} from "axios";

import ClientApi from "@/core/network/ClientApi";

import {NoticeEntity} from "@/core/entity/notice/NoticeEntity";
import {ServerResponse} from "@/core/entity/server/ServerResponse";

import {GetNoticesPort} from "@/core/repository/notice/port/GetNoticesPort";
import {INoticeRepository} from "@/core/repository/notice/INoticeRepository";
import {GetNoticeByIdPort} from "@/core/repository/notice/port/GetNoticeByIdPort";
import {CreateNoticePort} from "@/core/repository/notice/port/CreateNoticePort";
import AuthApi from "@/core/network/AuthApi";
import {DeleteNoticePort} from "@/core/repository/notice/port/DeleteNoticePort";

export default class NoticeRepository implements INoticeRepository {

    getPaginatedNotices(port: GetNoticesPort): Promise<AxiosResponse<ServerResponse<NoticeEntity[]>>> {

        const api = new ClientApi().getAxios();

        return api.get<ServerResponse<NoticeEntity[]>>("/notice", {
            params: {
                page: port.page
            }
        });
    }

    getNoticeById(port: GetNoticeByIdPort): Promise<AxiosResponse<ServerResponse<NoticeEntity>>> {
        const api = new ClientApi().getAxios();
        return api.get<ServerResponse<NoticeEntity>>(`/notice/${port.notice_id}`);
    }

    createNotice(port: CreateNoticePort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .post('/notice', {
                user_id: port.user_id,
                notice: port.notice,
                layouts: port.layouts
            });
    }

    deleteNotice(port: DeleteNoticePort): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .delete(`/notice/${port.notice_id}`);
    }
}