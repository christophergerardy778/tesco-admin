import {LoginPort} from "@/core/repository/auth/AuthPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import {AxiosResponse} from "axios";

export interface IAuthRepository {
    login(port: LoginPort): Promise<AxiosResponse<ServerResponse<UserEntity>>>;
}