import {IAuthRepository} from "@/core/repository/auth/IAuthRepository";
import {LoginPort} from "@/core/repository/auth/AuthPort";
import {AxiosResponse} from "axios";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {UserEntity} from "@/core/entity/auth/UserEntity";
import Client from "@/core/network/ClientApi";

export default class AuthRepository implements IAuthRepository {
    login(port: LoginPort): Promise<AxiosResponse<ServerResponse<UserEntity>>> {
        return new Client()
            .getAxios()
            .post("/auth/admin", {
                email: port.email,
                password: port.password
            });
    }

}