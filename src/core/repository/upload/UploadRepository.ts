import {AxiosResponse} from "axios";
import AuthApi from "@/core/network/AuthApi";
import {IUploadRepository} from "@/core/repository/upload/IUploadRepository";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IFile} from "@/core/entity/file/IFile";

export default class UploadRepository implements IUploadRepository {
    deleteFiles(fileIds: string[]): Promise<AxiosResponse<ServerResponse<string>>> {
        return new AuthApi()
            .getAxios()
            .delete("/upload", { data: fileIds });
    }

    uploadFiles(form: FormData): Promise<AxiosResponse<ServerResponse<IFile[]>>> {
        return new AuthApi()
            .getAxios()
            .post("/upload", form, { headers: { "Content-Type": "multipart/form-data"} });
    }
}