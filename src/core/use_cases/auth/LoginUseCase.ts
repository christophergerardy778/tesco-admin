import {UserEntity} from "@/core/entity/auth/UserEntity";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {LoginPort} from "@/core/repository/auth/AuthPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IAuthRepository} from "@/core/repository/auth/IAuthRepository";
import AuthRepository from "@/core/repository/auth/AuthRepository";

class LoginUseCase implements BaseUseCase<LoginPort, ServerResponse<UserEntity>> {

    private readonly repository!: IAuthRepository;

    constructor() {
        this.repository = new AuthRepository();
    }

    async execute(port: LoginPort): Promise<ServerResponse<UserEntity>> {
        try {
            const result = await this.repository.login(port);
            return result.data;
        } catch (e) {
            return {
                ok: false,
                error: "ACCESO DENEGADO"
            }
        }
    }
}

export default LoginUseCase;