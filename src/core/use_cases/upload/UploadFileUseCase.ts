import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {UploadFilesPort} from "@/core/repository/upload/port/UploadFilesPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IUploadRepository} from "@/core/repository/upload/IUploadRepository";
import UploadRepository from "@/core/repository/upload/UploadRepository";
import {IFile} from "@/core/entity/file/IFile";
import {AxiosError} from "axios";

class UploadFileUseCase
    extends HttpCodeHandler<IFile[]>
    implements BaseUseCase<UploadFilesPort, ServerResponse<IFile[]>>
{
    private repository!: IUploadRepository;

    constructor() {
        super();
        this.repository = new UploadRepository();
    }

    async execute(port: UploadFilesPort): Promise<ServerResponse<IFile[]>> {
        try {
            const result = await this.repository.uploadFiles(port.form);
            return result.data!;
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}

export default UploadFileUseCase;