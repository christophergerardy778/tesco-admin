import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IUploadRepository} from "@/core/repository/upload/IUploadRepository";
import UploadRepository from "@/core/repository/upload/UploadRepository";
import {DeleteFilePort} from "@/core/repository/upload/port/DeleteFilePort";
import {AxiosError} from "axios";

class DeleteFileUseCase
    extends HttpCodeHandler<string>
    implements BaseUseCase<DeleteFilePort, ServerResponse<string>>
{
    private repository!: IUploadRepository;

    constructor() {
        super();
        this.repository = new UploadRepository();
    }

    async execute(port: DeleteFilePort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.deleteFiles(port.files);
            return result.data!;
        }  catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}

export default DeleteFileUseCase;