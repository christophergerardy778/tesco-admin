import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {AttemptEntity} from "@/core/entity/attempt/AttemptEntity";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {GetAttemptsPort} from "@/core/repository/attempt/port/GetAttemptsPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IAttemptRepository} from "@/core/repository/attempt/IAttemptRepository";
import AttemptRepository from "@/core/repository/attempt/AttemptRepository";
import {AxiosError} from "axios";

class GetAttempts
    extends HttpCodeHandler<AttemptEntity[]>
    implements BaseUseCase<GetAttemptsPort, ServerResponse<AttemptEntity[]>>
{
    private readonly repository!: IAttemptRepository;

    constructor() {
        super();
        this.repository = new AttemptRepository();
    }

    async execute(port: GetAttemptsPort): Promise<ServerResponse<AttemptEntity[]>> {
        try {
            const result = await this.repository.getAttemptsByStatusAndPage(port);
            return this.handle(result);
        } catch (e) {
            const err = e as AxiosError;
            return this.handle(err.response!);
        }
    }
}

export default GetAttempts;