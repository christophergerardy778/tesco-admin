import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {UpdateAttemptPort} from "@/core/repository/attempt/port/UpdateAttemptPort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {IAttemptRepository} from "@/core/repository/attempt/IAttemptRepository";
import AttemptRepository from "@/core/repository/attempt/AttemptRepository";
import {AxiosError} from "axios";

class UpdateAttemptStatus
    extends HttpCodeHandler<string>
    implements BaseUseCase<UpdateAttemptPort, ServerResponse<string>>
{

    private readonly repository!: IAttemptRepository;

    constructor() {
        super();
        this.repository = new AttemptRepository();
    }

    async execute(port: UpdateAttemptPort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.updateAttemptStatus(port);
            return this.handle(result);
        } catch (e) {
            const err = e as AxiosError;
            return this.handle(err.response!);
        }
    }
}

export default UpdateAttemptStatus;



