import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {DeleteNoticePort} from "@/core/repository/notice/port/DeleteNoticePort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {INoticeRepository} from "@/core/repository/notice/INoticeRepository";
import NoticeRepository from "@/core/repository/notice/NoticeRepository";
import {AxiosError, AxiosResponse} from "axios";

class DeleteNoticeById extends HttpCodeHandler<string> implements BaseUseCase<DeleteNoticePort, ServerResponse<string>>{

    private readonly repository!: INoticeRepository;

    constructor() {
        super();
        this.repository = new NoticeRepository();
    }

    async execute(port: DeleteNoticePort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.deleteNotice(port);
            return this.handle(result);
        } catch (e) {
            const err = e as AxiosError;
            return this.handle(err.response!);
        }
    }

}

export default DeleteNoticeById;