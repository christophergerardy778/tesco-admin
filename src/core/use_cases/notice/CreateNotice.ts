import {HttpCodeHandler} from "@/core/shared/HttpCodeHandler";
import {BaseUseCase} from "@/core/shared/BaseUseCase";
import {CreateNoticePort} from "@/core/repository/notice/port/CreateNoticePort";
import {ServerResponse} from "@/core/entity/server/ServerResponse";
import {INoticeRepository} from "@/core/repository/notice/INoticeRepository";
import NoticeRepository from "@/core/repository/notice/NoticeRepository";
import {AxiosError} from "axios";

class CreateNotice extends HttpCodeHandler<string> implements BaseUseCase<CreateNoticePort, ServerResponse<string>>{

    private readonly repository!: INoticeRepository;

    constructor() {
        super();
        this.repository = new NoticeRepository();
    }

    async execute(port: CreateNoticePort): Promise<ServerResponse<string>> {
        try {
            const result = await this.repository.createNotice(port);
            return this.handle(result);
        } catch (e) {
            const error = e as AxiosError;
            return this.handle(error.response!);
        }
    }
}

export default CreateNotice;