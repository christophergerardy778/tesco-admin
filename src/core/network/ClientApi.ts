import { AxiosInstance } from "axios";
import AxiosConfig from "@/core/network/config/AxiosConfig";

export default class ClientApi {

    private readonly axiosInstance!: AxiosInstance;

    constructor() {
        this.axiosInstance = AxiosConfig;
    }

    public getAxios() {
        return this.axiosInstance;
    }

}
