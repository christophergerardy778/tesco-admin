import {UserEntity} from "@/core/entity/auth/UserEntity";

export enum STATUS_ATTEMPT {
    PENDING,
    REJECTED,
    ACCEPTED
}

export interface AttemptEntity {
    _id: string;
    status: STATUS_ATTEMPT,
    user: UserEntity,
    verificationFile: string;
    createdAt: string;
}