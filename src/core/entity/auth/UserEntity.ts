export interface UserEntity {
    _id?: string;
    name: string;
    lastname: string;
    email: string;
    password?: string;
    active: boolean;
    enrollment?: string;
    //roll: Role;
    graduate: boolean;
    dateOfAdmission?: string;
    egressDate?: string;
}

export default class User implements UserEntity {
    _id!: string;
    name!: string;
    active!: boolean;
    email!: string;
    graduate!: boolean;
    lastname!: string;
}