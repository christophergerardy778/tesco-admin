import {ILayout} from "@/core/entity/notice/layout/ILayout";

export interface NoticeEntity {
    _id: string;
    title: string;
    description: string;
    cover: string;
    createdAt: string;
    layouts: ILayout[];
}