export interface WebRouteEntity {
    text: string,
    icon: string,
    route: string
}