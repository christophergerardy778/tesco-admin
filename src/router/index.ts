import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Login from "@/views/Login.vue";
import Home from "@/views/Home.vue";
import store from '@/store';
import Notices from "@/views/Notices.vue";
import CreateNotice from "@/views/CreateNotice.vue";
import NoticeDetails from "@/views/NoticeDetails.vue";
import Attempts from "@/views/Attempts.vue";
import Settings from "@/views/Settings.vue";
import Access from "@/views/Access.vue";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: {
      authAccess: true
    }
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/anuncios',
    name: 'Notices',
    component: Notices,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/anuncio/:id',
    name: 'NoticeDetail',
    component: NoticeDetails,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/crear-anuncio',
    name: 'create-notice',
    component: CreateNotice,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/solicitudes',
    name: 'attempts',
    component: Attempts,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/configuraciones',
    name: 'settings',
    component: Settings,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/accesos',
    name: 'access',
    component: Access,
    meta: {
      requireAuth: true
    }
  }
  /*{
    path: '/about',
    name: 'About',
    component: () => import(/!* webpackChunkName: "about" *!/ '../views/About.vue')
  }*/
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.requireAuth)) {

    if (store.state.Auth.token.length > 0) next();
    else next('/')

  } else if (to.matched.some(route => route.meta.authAccess)) {

    if (store.state.Auth.token.length > 0) next({ name: 'Home' });
    else next();

  } else {
    next();
  }
});


export default router
